# DewTime
A todo list time tracker app for when you'll finish your tasks... in dew time... 😎.

https://dewti.me

# Usage
The input fields take a task name and a duration string.

## Duration String
Of the form `1h15m` where `h` stands for hours and `m` stands for minutes.
When no unit is specified minutes is assumed.
The input field should prevent any malformed specifiers.

## Completed Tasks
Completed tasks aren't automatically stopped.
Overtime is not displayed in any way.

## Daily Reset
A daily reset is applied using your computer's local time zone.
This removes all elapsed time on all configured tasks at the given time.
Whenever the reset time is updated, the reset is skipped for the day.
