import { browser } from "$app/env";
import { writable } from 'svelte/store';


function replacer(key, value) {
  if(value instanceof Map) {
    return {
      dataType: 'Map',
      value: Array.from(value.entries()), // or with spread: value: [...value]
    };
  } else {
    return value;
  }
}

function reviver(key, value) {
  if(typeof value === 'object' && value !== null) {
    if (value.dataType === 'Map') {
      return new Map(value.value);
    }
  }
  return value;
}

const getLocalStorage = (key, defaultValue) => {
	if (!browser) {
		return defaultValue;
	}
	let storeValue = window.localStorage.getItem(key);
	if (!storeValue) {
		return defaultValue;
	}
	try {
		return JSON.parse(storeValue, reviver);
	} catch(e) {
		return defaultValue;
	}
}

const subscribeHandler = (key) => {
	return (value) => {
		if (browser) {
			window.localStorage.setItem(key, JSON.stringify(value, replacer));
		}
	}
}

// Array item structure.
// {
//     id: Math.random().toString(),
//     name: todoName,
//     duration: durationHour * 60 * 60 + durationMin * 60,  // sec
//     elapsed: 0  // msec
//     notified: false
// }
export const todos = writable(getLocalStorage('todos', []))
todos.subscribe(subscribeHandler('todos'));

export const lastReset = writable(getLocalStorage('lastReset', 0));
lastReset.subscribe(subscribeHandler('lastReset'));

export const resetTime = writable(getLocalStorage('resetTime', '00:00'));
resetTime.subscribe(subscribeHandler('resetTime'));

export const sendNotifications = writable(getLocalStorage('sendNotifications', false));
sendNotifications.subscribe(subscribeHandler('sendNotifications'));
