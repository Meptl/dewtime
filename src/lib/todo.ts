import { todos } from '../stores.js';
import { get } from 'svelte/store';

type TodoItem = {
	id: string;
	name: string;
	isRunning: boolean;
	notified: boolean;
	duration: number; // seconds
	_elapsed: number; // msec
	_runStart: number;
}

export function resetAll() {
	stopAll();
	for (const todo of get(todos)) {
		todo._elapsed = 0;
		todo.notified = false;
	}
	applyReactivity();
}

export function add(name: string, duration: number) {
	let todoItem = get(todos);
	let id = Math.floor(Math.random() * 999999).toString();
	// Let's just hope we never have 999999 todo items...
	while (todoItem.find((e: TodoItem) => e.id == id)) {
		id = Math.floor(Math.random() * 999999).toString();
	}

	const newTodo: TodoItem = {
		id: id,
		name: name,
		duration: duration,
		_elapsed: 0,
		_runStart: 0,
		isRunning: false,
		notified: false,
	}
	todoItem.push(newTodo);
	applyReactivity();
}

export function remove(id: string) {
	let todoItem = get(todos);
	for (var i = 0; i < todoItem.length; i++) {
		if (todoItem[i].id == id) {
			todoItem.splice(i, 1);
			break;
		}
	}
	applyReactivity();
}

export function toggleTimer(id: string) {
	let todoItem = get(todos).find((e: TodoItem) => e.id == id);
	let isRunning = todoItem.isRunning;
	stopAll();

	todoItem.isRunning = !isRunning;
	if (todoItem.isRunning) {
		todoItem._runStart = Date.now();
	}

	applyReactivity();
}

export function stop(id: string) {
	let todoItem = get(todos).find((e: TodoItem) => e.id == id);
	_stop(todoItem);
	applyReactivity();
}

export function elapsed(id: string) {
	let todoItem = get(todos).find((e: TodoItem) => e.id == id);
	return itemElapsed(todoItem);
}

export function progress(id: string) {
	let todoItem = get(todos).find((e: TodoItem) => e.id == id);
	return itemProgress(todoItem);
}

export function itemElapsed(item: TodoItem) {
	if (item.isRunning) {
		return item._elapsed + (Date.now() - item._runStart);
	} else {
		return item._elapsed;
	}
}

export function itemProgress(item: TodoItem) {
    let p;
    if (item.duration == 0) {
      p = 1.0;
    } else {
      p = itemElapsed(item) / 1000 / item.duration;
      if (p > 1.0) {
        p = 1.0;
      }
    }
    return p;
}

export function notified(id: string, value: boolean) {
	let todoItem = get(todos).find((e: TodoItem) => e.id == id);
	todoItem.notified = value;
	applyReactivity();
}

function stopAll() {
	for (const todo of get(todos)) {
		_stop(todo);
	}
}

function _stop(item: TodoItem) {
	if (item.isRunning) {
		item._elapsed += Date.now() - item._runStart;
	}
	item.isRunning = false;
}

function applyReactivity() {
	todos.set(get(todos));
}
