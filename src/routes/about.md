<svelte:head>
  <title>DewTime - About</title>
</svelte:head>

# DewTime
A todo list time tracker app for when you'll finish your tasks... in dew time... 😎.
This app stores todo information on your browser's [LocalStorage](https://developer.mozilla.org/en-US/docs/Web/API/Web_Storage_API).

# Author's Workflow
My workflow involves mixing this app with a hardware [pomodoro](https://en.wikipedia.org/wiki/Pomodoro_Technique) device.
I create tasks for each category of work I want to perform daily.
During pomodoro breaks, I ask myself how I'm feeling on the current task and whether I'm okay with continuing working on it for another session.


I have no issue overshooting my daily alotted time blocks, hence why there is no
"completion" events on a task, and why additional time on a task has no visual
representation in the app.
